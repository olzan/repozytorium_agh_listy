CC=gcc
CFLAGS=-Wall
LIBS=-lm
listy: listy.o klient.o
   $(CC) -Wall -o listy listy.o klient.o $(LIBS)
listy.o: listy.c moduly.h
    $(CC) -Wall -c listy.c

klient.o: klient.c
    $(CC) -Wall -c klient.c